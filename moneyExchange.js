//input:giá trị số tiền USD, số tiền VND tương đương 1 USD
//progress: Tính số tiền VND tương đương = số tiền USD * số tiền VND tương đương 1 USD
//output: Số tiền VND sau khi quy đổi

function moneyExchange() {
  // Lấy giá trị số tiền USD từ ô input
  var usd = parseFloat(document.getElementById("usd").value);

  // Tính số tiền VND tương đương
  var vnd = usd * 23500;

  // Hiển thị kết quả quy đổi lên màn hình
  document.getElementById("result").innerHTML =
    "Số tiền quy đổi sang VND là: " + vnd + " VND";
}
