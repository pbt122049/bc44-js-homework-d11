//input: số ngày làm việc, lương 1 ngày làm việc
/*progress:
dom giá trị số ngày làm ng dùng nhập vào
khai báo lương 1 ngày
khai báo kết quả = số ngày x lương 1 ngày
*/
//output: xuất kết quả vào thẻ p có id là resultSalary

function calculateSalary() {
  //số ngày làm việc
  const days = document.getElementById("workingDays").value;
  //lương 1 ngày làm việc
  const dailySalary = 100000;
  //Lương = số ngày x lương 1 ngày
  const totalSalary = days * dailySalary;
  //Xuất kết quả ra màn hình
  document.getElementById("resultSalary").textContent = `Tổng lương cho ${days} ngày làm việc là ${totalSalary} đ.`;
}
